<?php
/**
 * Created by PhpStorm.
 * User: likhojiba
 * Date: 2018/06/10
 * Time: 2:01 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ['id'];

    public function rules($id = null) {
        return $rules = [
            'name' => 'required|min:3',
            'price' => 'required',
            'slug' => 'sometimes|required|unique:products,slug,' . $id
        ];
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }
}