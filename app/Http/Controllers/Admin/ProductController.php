<?php
/**
 * Created by PhpStorm.
 * User: likhojiba
 * Date: 2018/06/10
 * Time: 12:16 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Validator;

class ProductController extends Controller
{
    protected $product;
    public function __construct(Product $productModel)
    {
        $this->product = $productModel;
    }

    public function create() {

        $data['page_heading'] = 'Add a New Product';
        return view('admin.products.create', $data);
    }

    public function store(Request $request) {

        $input = $request->all();
        $slug = strtolower(str_replace(' ', '-', $request->name));
        $input['slug'] = $slug;

        $validator = Validator::make($input, $this->product->rules());

        if ($validator->fails()) {
            return redirect('products.create')->withErrors($validator)->withInput();
        } else {
           $this->product->create($input);
            session()->flash('status', 'The product has been added !!');
            return redirect('home');
        }
    }

    public function getEdit($id) {

        $product = $this->product->find($id);
        $data['product'] = $product;
        $data['page_heading'] = 'Edit : ' . $product->name;

        return view('admin.products.create', $data);
    }

    public function postEdit(Request $request, $id) {

        $input = $request->all();
        $slug = strtolower(str_replace(' ', '-', $request->name));
        $input['slug'] = $slug;

        $validator = Validator::make($input, $this->product->rules($id));


        $updateProduct = $this->product->find($id);
        if ($validator->fails()) {
            return redirect('admin/products/edit/'.$id)->withErrors($validator)->withInput();
        } else {
            $updateProduct->update($input);
            session()->flash('status', 'The product has been updated !!');
            return redirect('home');
        }

    }

    public function delete($id) {

         $this->product->find($id)->delete();
    }

    public function deactivate($id) {

        $this->product->find($id)->update(['is_active' => 0]);
    }
}