<?php
/**
 * Created by PhpStorm.
 * User: likhojiba
 * Date: 2018/06/10
 * Time: 4:44 PM
 */

namespace App\Http\Controllers;


use App\Models\Product;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class CustomerController extends Controller
{
    protected $transaction;
    protected $product;
    public function __construct(Transaction $transaction, Product $product)
    {
        $this->transaction = $transaction;
        $this->product = $product;
    }

    /**
     * Top Up user account
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function topUpAccount () {

        $data['user'] = Auth::user();
        $data['page_heading'] = 'Top Up Your Account';

        return view('customers.top_up', $data);
    }

    public function postTopUp(Request $request) {

        $user = Auth::user();
        $request->validate([
            'amount_available' => 'required',
        ]);

        $user->update(['amount_available' => $request->amount_available]);

        $transactioData = [
            'user_id' => $user->id,
            'type' => 'top-up',
            'amount_before' => $request->old_amount,
            'amount_after' => $request->amount_available
        ];
        //Insert into transactions table
        $this->saveTransaction($transactioData);

        session()->flash('status', 'Your spending amount has been topped up !!');
        return redirect('home');
    }

    public function saveTransaction($data) {
        $this->transaction->create($data);
    }

    /**
     * Store Cart status in session
     */
    public function storeCartInSession($productId) {

        return session(['cart_amount' => 1, 'cart_product' => $productId]);
    }

    /**
     * Clear shopping Cart
     */
    public function clearShoppingCart(Request $request) {
         $request->session()->forget(['cart_amount', 'cart_product']);
    }

    public function getCheckOut(Request $request) {

        $productID = $request->session()->get('cart_product');
        $selectedProduct = $this->product->find($productID);

        $data['page_heading'] = 'Complete purchase';
        $data['priceAfterDiscount'] = $this->calculateDiscount($selectedProduct->price);
        $data['product'] = $selectedProduct;

        return view('customers.shopping_cart', $data);
    }

    public function calculateDiscount($price) {

        switch ($price) {
            case in_array($price, range(50, 100)):
                $discountPercent = 0.0;
                break;
            case in_array($price, range(112, 115)):
                $discountPercent = 0.25;
                break;
            case $price > 120:
                $discountPercent = 0.50;
                break;
            default:
                $discountPercent = 0;
        }

        $discountValue =  $price * $discountPercent;
        $finalPrice = $price - $discountValue;
        return $finalPrice;

    }


    public function postCheckOut(Request $request) {

        $transactioData = [
            'user_id' => Auth::user()->id,
            'type' => 'purchase',
            'product_id' => $request->product_id,
            'amount_before' => $request->amount_before,
            'amount_after' => $request->amount_after
        ];

        //Subtract this total from user available
        $newUserAmount = Auth::user()->amount_available - $request->amount_after;
        User::find(Auth::user()->id)->update(['amount_available' => $newUserAmount]);
        //Insert into transactions table
        $this->saveTransaction($transactioData);
        $request->session()->forget(['cart_amount', 'cart_product']);

        session()->flash('status', 'Product purchased, continue shopping !!');
        return redirect('home');

    }

    public function getTransactions() {

        $transactions = $this->transaction->where('user_id', Auth::user()->id)->get();
        $completeTransactions = [];
        foreach ($transactions as $trans) {
            $productName = ($trans->product_id != null) ? $this->product->find($trans->product_id)->name : '';
            $completeTransactions[] = [
                'user_id' => $trans->user_id,
                'type' => $trans->type,
                'product' => $productName,
                'amount_before' => $trans->amount_before,
                'amount_after' => $trans->amount_after
            ];
        }
        $data['transactions'] = $completeTransactions;
        $data['page_heading'] = 'View you transactions';

        return view('customers.transactions', $data);
    }
}