<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $product;
    public function __construct(Product $product)
    {
        $this->middleware('auth');
        $this->product = $product;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->is_admin) {
            $data['page_heading'] = 'Manage Products';
            $data['view'] = 'admin_products';
            $data['products'] = $this->product->get();
        } else {
            $data['page_heading'] = 'Manage Products and Transactions';
            $data['view'] = 'customer_products';
            $data['products'] = $this->product->active()->get();
        }
        return view('home', $data);
    }
}
