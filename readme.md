## How to set up

- Please make sure you are running PHP >= 7.1.3 and have mysql installed
- Clone the repo
- cd into the project folder
- Run cp .env.example .env
- After cloning run composer-install
- When that is complete set up you local database details in the .env
- Run php artisan migrate
- Run php artisan db:seed (To populate login details for admin user)
- Run php arttisan key:gen to generate application key
- Run php artisan serve to run the app
- After that go to the login page - use likhojiba@gmail.com as your username, password Likho@1990
- Once logged in start adding products in the backend 
- When you have added products logout and register to get normal user access which will allow you to buy products, top up and view transactions

