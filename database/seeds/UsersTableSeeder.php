<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Likho',
            'email' => 'likhojiba@gmail.com',
            'password' => bcrypt('Likho@1990'),
            'is_admin' => 1
        ]);
    }
}
