<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::prefix('admin')->group(function () {
        Route::get('/products/create', 'Admin\ProductController@create')->name('products.create');
        Route::post('/products/store', 'Admin\ProductController@store')->name('products.post');

        Route::get('/products/edit/{id}', 'Admin\ProductController@getEdit')->name('products.edit');
        Route::post('/products/update/{id}', 'Admin\ProductController@postEdit')->name('products.update');

        Route::delete('/products/delete/{id}', 'Admin\ProductController@delete')->name('products-delete');

        Route::post('/products/deactivate/{id}', 'Admin\ProductController@deactivate')->name('products-deactivate');
    });


    Route::get('/account/top-up', 'CustomerController@topUpAccount')->name('account.top-up');
    Route::post('/account/post-top-up', 'CustomerController@postTopUp')->name('account.post-top-up');

    Route::get('/store-cart-in-session/{productId}', 'CustomerController@storeCartInSession');
    Route::get('/clear-cart', 'CustomerController@clearShoppingCart');
    Route::get('/products/checkout', 'CustomerController@getCheckOut')->name('products.checkout');
    Route::post('/products/complete-purchase', 'CustomerController@postCheckOut')->name('products.complete-purchase');

    Route::get('/transactions', 'CustomerController@getTransactions')->name('transactions.list');
});

//product-buy
