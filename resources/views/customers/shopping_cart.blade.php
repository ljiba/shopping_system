@extends('layouts.app')

@section('content')
    <div class="col-md-8">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">{{ $product->name }}</h4>
                <p class="card-text p-y-1">{{ $product->description }}</p>
                <span>Amount Before Discount: R {{ money_format("%i", $product->price) }}</span>&nbsp; | &nbsp;
                <span>Amount After Discount: R {{ $priceAfterDiscount }} }}</span>
            </div>
        </div>
        <br />
        @if (Auth::user()->amount_available > $priceAfterDiscount)
            <form method="POST" action="{{ route('products.complete-purchase') }}">
                @csrf
                <input type="hidden" name="product_id" value="{{ $product->id }}" />
                <input type="hidden" name="amount_before" value="{{ $product->price }}" />
                <input type="hidden" name="amount_after" value="{{ $priceAfterDiscount }}" />
                <button class="btn btn-success">Complete Purchase</button>
            </form>

        @else
            <span class="text-danger">You do not have enough for this transaction..</span>
        @endif
    </div>
@endsection


