@extends('layouts.app')

@section('content')
        <form method="POST" action="{{ route('account.post-top-up') }}">
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $user->name }}" readonly />
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" value="{{ $user->email }}" readonly />
            </div>
            <div class="form-group">
                <label for="old_amount">Amount Available <small> (ZAR)</small></label>
                <input type="text" class="form-control {{ $errors->has('amount_available') ? ' is-invalid' : '' }} price" name="old_amount" value="{{ $user->amount_available }}" readonly />
            </div>
            <div class="form-group">
                <label for="amount_available">New Amount <small> (ZAR)</small> - Must be greater than current amount</label>
                <input type="text" class="form-control {{ $errors->has('amount_available') ? ' is-invalid' : '' }} price" name="amount_available" value="" />
                @if ($errors->has('amount_available'))
                    <span class="invalid-feedback"><strong>{{ $errors->first('amount_available') }}</strong></span>
                @endif
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
         </form>
@endsection
