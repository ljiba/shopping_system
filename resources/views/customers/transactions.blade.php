@extends('layouts.app')

@section('content')
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">UserId</th>
            <th scope="col">Type</th>
            <th scope="col">Product</th>
            <th scope="col">Amount Before</th>
            <th scope="col">Amount After</th>
        </tr>
        </thead>
        <tbody>

        @foreach($transactions as $transaction)
            <tr>
                <th>{{ $transaction['user_id'] }}</th>
                <th>{{ $transaction['type'] }}</th>
                <th>{{ $transaction['product'] }}</th>
                <td>R{{ money_format("%i", $transaction['amount_before']) }}</td>
                <td>R{{ money_format("%i", $transaction['amount_after']) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
