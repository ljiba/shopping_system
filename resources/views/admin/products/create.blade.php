@extends('layouts.app')

@section('content')
    @if (isset($product))
    <form method="POST" action="{{ route('products.update', $product->id) }}">
    @else
    <form method="POST" action="{{ route('products.post') }}">
    @endif
        @csrf
        @if ($errors->has('slug'))
            <div class="alert alert-danger">
                There is already a product with the given name...
            </div>
        @endif
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="@isset($product) {{ $product->name }} @else {{ old('name') }} @endisset" placeholder="Enter product name" required autofocus />
            @if ($errors->has('name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="price">Price</label>
            <input type="text" class="form-control {{ $errors->has('price') ? ' is-invalid' : '' }} price" name="price" value="@isset($product) {{ $product->price }} @else {{ old('price') }} @endisset" />
            @if ($errors->has('price'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('price') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" rows="3" value="" name="description">@isset($product) {{ $product->description }} @else {{ old('description') }} @endisset</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection



