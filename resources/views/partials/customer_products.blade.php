<div class="row hidden-md-up">
    @foreach($products as $product)
    <div class="col-md-4">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">{{ $product->name }}</h4>
                <p class="card-text p-y-1">{{ $product->description }}</p>
                <a href="#" class="card-link buy-product" id="{{ $product->id }}">Add to Cart</a>&nbsp; | &nbsp;
                <span>Price: R{{ money_format("%i", $product->price) }}</span>
            </div>
        </div>
    </div>
    @endforeach
</div>