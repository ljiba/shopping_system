<a href="{{ route('products.create') }}" class="btn btn-success pull-right">Add New Product</a>
<div class="clearfix"></div>
<br />
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Price</th>
        <th scope="col">Status</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
    <tr>
        <th>{{ $product->name }}</th>
        <td>R{{ money_format("%i", $product->price) }}</td>
        <td>
            @if ($product->is_active == 1)
                <span class="badge badge-pill badge-success">Active</span>
            @else
                <span class="badge badge-pill badge-danger">Inactive</span>
            @endif
        </td>
        <td>
            <a href="{{ route('products.edit', $product->id) }}" class="btn-sm btn-success">Edit</a> &nbsp;| &nbsp;<button class="btn-sm btn-danger delete-product" id="{{ $product->id }}" data-token="{{ csrf_token() }}">Delete</button> &nbsp;| &nbsp;
            <a href="#" class="btn-sm btn-warning deactivate-product" id="{{ $product->id }}" data-token="{{ csrf_token() }}">Deactivate</a>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>