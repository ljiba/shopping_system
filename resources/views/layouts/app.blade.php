<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @else
                            @if(Auth::user()->is_admin == 0)
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>Cart <span class="badge" id="cart-amount">{{ Session::get('cart_amount', 0) }}</span></a>
                                @if (Session::has('cart_amount'))
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item clear-cart" href="#">Clear Cart</a>
                                        <a class="dropdown-item" href="{{ route('products.checkout') }}">
                                            Got to CheckOut
                                        </a>
                                    </div>
                                @endif
                            </li>
                            @endif
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    @if (null != Auth::user())
                    <div class="col-md-2">
                        <ul class="list-group list-group-flush">
                            @if (Auth::user()->is_admin == 1)
                            <li class="list-group-item">
                                <a href="{{ route('home') }}">Manage Products</a>
                            </li>
                            @else
                                <li class="list-group-item">
                                    Credit Available <br />
                                    R{{ money_format("%i", Auth::user()->amount_available) }}
                                </li>
                                <li class="list-group-item">
                                    <a href="{{ route('home') }}">Home</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{ route('account.top-up') }}">TopUp Account</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="{{ route('transactions.list') }}">View Transactions</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                    @endif
                    <div class="col-md-10">
                        <div class="card">
                            <div class="card-header">
                                @if (\Illuminate\Support\Facades\Auth::user())
                                {{ $page_heading }}
                                @endif
                            </div>
                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <script src="{{ asset('js/jquery-mask-as-number.js') }}" defer></script>
    <script>
        $(document).ready(function () {

            $('.price').maskAsNumber();

            $(".delete-product").click(function (e) {
                e.preventDefault();
                if (confirm("Sure you want to delete?")) {
                    var productID = $(this).attr('id');
                    var token = $(this).data('token');
                    $.ajax({
                        type: "POST",
                        url: '/admin/products/delete/' + productID,
                        data: {_method: 'delete', _token :token},
                        success: function () {
                            alert("Deleted");
                            location.reload();
                        }
                    });
                }
                return false;
            });

            $(".deactivate-product").click(function (e) {
                e.preventDefault();
                if (confirm("Please note product will not display on frontend.")) {
                    var productID = $(this).attr('id');
                    var token = $(this).data('token');
                    $.ajax({
                        type: "POST",
                        url: '/admin/products/deactivate/' + productID,
                        data: {_token :token},
                        success: function () {
                            alert("Deactivated");
                            location.reload();
                        }
                    });
                }
                return false;
            });

            //Customer functions

            $(".buy-product").click(function () {
                // cart-amount
                currentlyInCart = $('#cart-amount').text();
                if (currentlyInCart == '' || currentlyInCart == 0) {
                    $('#cart-amount').text(1);
                    $('#count_products').val(1);
                    var productID = $(this).attr('id');
                    $.ajax({
                        method: "GET",
                        url: "store-cart-in-session/"+productID,
                        success: function(){
                            location.reload();
                        }
                    });
                } else {
                    alert("You can only buy one product at a time, Please checkout first.");
                    return false;
                }
            });

            $('.clear-cart').click(function () {
                $.ajax({
                    method: "GET",
                    url: "clear-cart",
                    success: function(){
                        location.reload();
                    }
                });
            });

        });
    </script>
</body>
</html>
